#Spiral print of matrix 
using System;

public class Program{
// Function to print matrix in spiral form
    static void spiralPrint(int m, int n, int [,]a){
        int i, k = 0, l = 0;
        /* k - starting row index
        m - ending row index
        l - starting column index
        n - ending column index
        i - iterator
        */

        while (k < m && l < n){
            // Print the first row from the remaining rows
            for (i = l; i < n; ++i){
                Console.Write(a[k, i] +" ");
            }
            k++;
            // Print the last column from the remaining columns
            for (i = k; i < m; ++i){
                Console.Write(a[i,n - 1]+" ");
            }
            n--;

            // Print the last row from the remaining rows */
            if ( k < m){
                for (i = n-1; i >= l; --i){
                    Console.Write(a[m - 1, i]+" ");
                }
                m--;
            }

            // Print the first column from the remaining columns */
            if (l < n){
                for (i = m-1; i >= k; --i){
                    Console.Write(a[i, l] + " ");
                }
                l++;
            }
        }
    }
    public static void Main(){
        int R = 3;
        int C = 6;
        int [,]a = { {75, 85, 95, 65, 55, 45},
        {69, 59, 49, 39, 29, 19},
        {67, 57, 47, 37, 27, 17}
        };
        spiralPrint(R,C,a);
    }
}