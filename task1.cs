#Find factorial task
using System;

public class Program{
    public static void Main(){
        int faktorial = 1;
        Console.WriteLine("Enter Number");
        int number = Convert.ToInt32(Console.ReadLine());
        for(faktorial = 1; faktorial <=number; faktorial++){
            Console.Write(faktorial + "! = ");
            int answer = 1;
            for(int i = 1; i<=faktorial; i++){
                answer = answer*i;
            }
        Console.Write(answer);
        Console.Write("\n");
        }
    }
}